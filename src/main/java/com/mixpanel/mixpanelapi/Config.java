package com.mixpanel.mixpanelapi;

/* package */ class Config {
    public static final String BASE_ENDPOINT = "https://analytics.niki-ai.com";
    public static final int MAX_MESSAGE_SIZE = 50;
}
